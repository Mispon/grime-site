'use strict'

import React from 'react'

class Preview extends React.Component {
    constructor(props) {
        super(props)
    }
    render() {
        let src = this.props.image ? this.props.image : '/assets/images/smooth-dude.png'
        return(
            <div className="preview">
                <img src={src} />
            </div>
        )
    }
}

module.exports = Preview