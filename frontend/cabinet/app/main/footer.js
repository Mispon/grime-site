'use sctirct'

import '../../../styles/cabinet/footer.less'

import React from 'react'

import { formatDate, toMoney } from '../utils/formatter'

class Footer extends React.Component {
    constructor(props) {
        super(props)
    }
    hasLastLogin() {
        if (!this.props.lastLogin) {
            return false
        }
        return this.props.lastLogin !== '0001-01-01T00:00:00'
    }
    render() {        
        return(
            <div className="app-page-footer">
                {this.hasLastLogin() && <p className="last-login">Последний вход: {formatDate(this.props.lastLogin, true)}</p>}
                <p className="balance">Баланс: {toMoney(this.props.balance)} рублей</p>
                <p className="info">Не забывайте выходить с сервера перед пополнением баланса и покупками в магазине</p>
            </div>
        )
    }
}

module.exports = Footer