'use strict'

import '../../../styles/cabinet/shop.less'

import React from 'react'
import { Switch, Route } from 'react-router';
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { isUndefined } from 'util'

import Preview from '../main/preview'
import Footer from '../main/footer'

class Shop extends React.Component {
    constructor(props) {
        super(props)
    }
    componentWillMount() {
        if (isUndefined(this.props.userInfo)) {
            const email = localStorage.getItem('email')
            axios.get(`/app/api/account/getInfo?email=${email}`).then(response => {
                this.props.dispatch({
                    type: 'SET_DATA',
                    state: {userInfo: response.data}
                })
            })   
        }        
    }
    render() {
        if (isUndefined(this.props.userInfo)) {
            return null
        }
        return(
            <div className="app-page">
                <Preview />
                <div className="page shop">
                    <ul>
                        <li><Link to='/shop/premium'>ПРЕМИУМ АККАУНТ</Link></li>
                        <li><Link to='/shop/name-change'>СМЕНА НИКА</Link></li>
                        <li><Link to='/shop/name-color'>ЦВЕТ НИКА</Link></li>
                        <li><Link to='/shop/phone-number'>НОМЕР ТЕЛЕФОНА</Link></li>
                        <li><Link to='/shop/skin-reset'>СБРОС ВНЕШНОСТИ</Link></li>
                        <li><Link to='/shop/temp-skin'>ВРЕМЕННЫЙ СКИН</Link></li>
                    </ul>
                </div>
                <Footer balance={this.props.userInfo.get('Balance')} />            
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        userInfo: state.get('userInfo')
    }
}

module.exports = connect(mapStateToProps)(Shop)