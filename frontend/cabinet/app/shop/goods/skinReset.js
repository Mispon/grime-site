'use sctirct'

import '../../../../styles/cabinet/goods/skinReset.less'

import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { isUndefined } from 'util'

import actions from '../../store/actions'
import Preview from '../../main/preview'
import Alert from '../alert'
import Footer from '../../main/footer'
import {toMoney} from '../../utils/formatter'

class SkinReset extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            price: 300,
            showAlert: false,
            alertMessage: '',
            actionSuccess: false
        }
    }
    componentWillMount() {
        if (isUndefined(this.props.userInfo)) {
            const email = localStorage.getItem('email')
            axios.get(`/app/api/account/getInfo?email=${email}`).then(response => {
                this.props.dispatch({
                    type: 'SET_DATA',
                    state: {userInfo: response.data}
                })
            })   
        }
    }
    resetSkin() {
        if (this.props.userInfo.get('Balance') < this.state.price) {
            this.setAlertState({Success: false, Message: 'На балансе недостаточно средств'})
            return
        }
        const accountId = this.props.userInfo.get('AccountId')
        axios.post(`/app/api/shop/resetSkin?accountId=${accountId}&cost=${this.state.price}`)
            .then(response => {
                this.setAlertState(response.data)
                if (response.data.Success) {
                    this.props.updateState({price: this.state.price})
                }
            })
            .catch(error => this.setAlertState({Success: false, Message: 'Неполадки на сервере'}))
    }
    setAlertState(data) {
        this.setState({
            showAlert: true,
            actionSuccess: data.Success,
            alertMessage: data.Message
        })
    }
    render() {
        if (isUndefined(this.props.userInfo)) {
            return null
        }
        return(
            <div className="app-page">
                <Preview />
                <div className='page skin-reset-page'>
                    {this.state.showAlert && <Alert success={this.state.actionSuccess} message={this.state.alertMessage} />}
                    <p className="good-name">СБРОС ВНЕШНОСТИ</p>
                    <p className="price">{this.state.price} рублей</p>
                    <div className="description">
                        <p>Полностью сбрасывает настройки внешности вашего персонажа.</p>
                        <p>При следующем входе на сервер, вам вновь станет доступно окно настроек.</p>
                        <p>Таким образом, Вы сможете заново выбрать пол, прическу, имя и все остальные характеристики.</p>
                        <p>При этом вы <b>не теряете</b> игровой прогресс и имущество.</p>
                    </div>
                    <input 
                        className='buy-button' 
                        type='button' 
                        onClick={() => this.resetSkin()} 
                        value='Купить'
                    />
                </div>
                <Footer balance={this.props.userInfo.get('Balance')} />
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        userInfo: state.get('userInfo')
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        updateState: actions.updateState,
        dispatch: dispatch
    }, dispatch)
}

module.exports = connect(mapStateToProps, mapDispatchToProps)(SkinReset)