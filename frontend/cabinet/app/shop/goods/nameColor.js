'use sctirct'

import '../../../../styles/cabinet/goods/nameColor.less'

import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { isUndefined } from 'util'

import actions from '../../store/actions'
import ColorPicker from '../../utils/colorPicker'
import Preview from '../../main/preview'
import Footer from '../../main/footer'
import Alert from '../alert'
import {toMoney} from '../../utils/formatter'

class NameColor extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            red: 0,
            green: 0,
            blue: 0,
            price: 100,
            showAlert: false,
            alertMessage: '',
            actionSuccess: false
        }
    }
    componentWillMount() {
        if (isUndefined(this.props.userInfo)) {
            const email = localStorage.getItem('email')
            axios.get(`/app/api/account/getInfo?email=${email}`).then(response => {
                this.props.dispatch({
                    type: 'SET_DATA',
                    state: {userInfo: response.data}
                })
                this.setColorState()
            })   
        }
        else {
            this.setColorState()
        }
    }
    setColorState() {
        let nameColor = this.props.userInfo.get('NameColor')
        let rgb = nameColor ? nameColor.split(';') : [0, 0, 0]
        this.setState({red: rgb[0], green: rgb[1], blue: rgb[2]})
    }
    onPickerChange(state) {
        this.setState(state)
    }
    buyColor() {
        if (this.props.userInfo.get('Balance') < this.state.price) {
            this.setAlertState({Success: false, Message: 'На балансе недостаточно средств'})
            return
        }
        const accountId = this.props.userInfo.get('AccountId')
        let color = `${this.state.red};${this.state.green};${this.state.blue}`
        axios.post(`/app/api/shop/changeColor?accountId=${accountId}&cost=${this.state.price}&color=${color}`)
            .then(response => {
                this.setAlertState(response.data)
                if (response.data.Success) {
                    this.props.updateState({price: this.state.price, color: color})
                }
            })
            .catch(error => this.setAlertState({Success: false, Message: 'Неполадки на сервере'}))
    }
    setAlertState(data) {
        this.setState({
            showAlert: true,
            actionSuccess: data.Success,
            alertMessage: data.Message
        })
    }
    render() {
        if (isUndefined(this.props.userInfo)) {
            return null
        }
        let name = this.props.userInfo.get('Name')
        if (!name) {
            name = 'Неизвестный'
        }     
        const color = `rgb(${this.state.red}, ${this.state.green}, ${this.state.blue})`
        return(
            <div className="app-page">
                <Preview />
                <div className='page name-color-page'>
                    {this.state.showAlert && <Alert success={this.state.actionSuccess} message={this.state.alertMessage} />}
                    <p className="good-name">ЦВЕТ НИКА</p>
                    <p className="price">{this.state.price} рублей</p>
                    <ColorPicker colorHandler={this.onPickerChange.bind(this)} />
                    <p className="name" style={{color: color}}>{name}</p>
                    <input className="buy-button" type='button' onClick={() => this.buyColor()} value='Купить' />
                </div>
                <Footer balance={this.props.userInfo.get('Balance')} />
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        userInfo: state.get('userInfo')
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        updateState: actions.updateState,
        dispatch: dispatch
    }, dispatch)
}

module.exports = connect(mapStateToProps, mapDispatchToProps)(NameColor)