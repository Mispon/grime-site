'use strict'

import { Map } from 'immutable'
import { userInfo } from 'os';

const Reducer = (state = Map(), action) => {
    switch (action.type) {
        case 'SET_DATA':
            state = state.merge(action.state)
            break
        case 'UPDATE_STATE':
            state = state.updateIn(['userInfo', 'Balance'], value => value - action.price)
            if (action.name) {
                state = state.updateIn(['userInfo', 'Name'], value => value = action.name)
            }
            if (action.number) {
                state = state.updateIn(['userInfo', 'Phone'], value => value = action.number)
            }
            if (action.color) {
                state = state.updateIn(['userInfo', 'NameColor'], value => value = action.color)
            }
            break
        default:
            return state
    }
    return state   
}

export default Reducer