'use strict'

import {isEmailValid} from '../help/validator'
import {showError} from '../help/errorShower'

function register(e) {
    let email = document.querySelector('.email').value    
    let password = document.querySelector('.password').value
    let rePassword = document.querySelector('.repassword').value
    let referal = document.querySelector('.referal').value
    if (!inputValid(email, password, rePassword)) {
        return
    }
    let captcha = grecaptcha.getResponse()
    if (!captcha) {
        showError('Заполните капчу')
        return
    }
    setButtonState(true)
    axios.post(`/app/api/account/register?email=${email}&password=${password}&referal=${referal}`)
        .then(response => {            
            if (response.data.Success) {
                showSuccessForm()
            }
            else {
                showError(response.data.Message)
                setButtonState(false)
            }            
        })
        .catch(e => {
            showError('Произошла ошибка на сервере. Обратитесь к администрации')
            setButtonState(false)
        })
}

function inputValid(email, password, rePassword) {
    if (!(email && password && rePassword)) {
        showError('Заполните все обязательные поля')
        return false
    }
    if (!isEmailValid(email)) {
        showError('Указана некорректная почта')
        return false
    }
    if (password.length < 6) {
        showError('Пароль не должен быть короче 6 символов')
        return false
    }
    if (password !== rePassword) {
        showError('Пароли не совпадают')
        return false
    }
    return true
}

const showSuccessForm = () => {
    let form = document.querySelector('.form')
    form.style.display = 'none'
    let successForm = document.querySelector('.success-form')
    successForm.style.display = 'block'    
}

const setButtonState = (loading) => {
    let button = document.querySelector('.register-btn')
    let value = loading ? '<i class="fa fa-spinner fa-spin fa-fw"></i>' : 'Зарегистрироваться'
    let padding = loading ? '1vh 5.6vw' : '1vh 1.5vw'
    button.innerHTML = value
    button.style.padding = padding
}

module.exports = register