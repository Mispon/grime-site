'use strict'

window.onload = () => {
    axios.post('/app/api/news/getnews')
        .then(response => {            
            let content = JSON.parse(response.data.content)
            let items = content.response.items
            showNews(items)
        })
        .catch(error => console.log('Не удалось загрузить новости'))
}

function showNews(items) {
    let news = filterItems(items)
    let feed = document.querySelector('.news-feed')
    news.forEach(item => {        
        let div = document.createElement('div')
        appendHeader(div, item.date)
        appendBody(div, item)
        appendStatistic(div, item)
        div.setAttribute('class', 'wrap-text')
        let li = document.createElement('li')
        li.appendChild(div)
        feed.appendChild(li)
    })
}

function filterItems(items) {
    const newsMark = '#oбновление'
    let result = []
    items.forEach(item => {
        if (item.text.match(/#обновление/i)) {
            let isArticle = ('link' in item.attachments[0])
            result.push({
                text: item.text,
                likes: item.likes.count,
                views: item.views.count,
                date: item.date,
                link: isArticle ? item.attachments[0]['link'].url : '',
                image: isArticle 
                    ? item.attachments[0]['link']['photo']['photo_604']
                    : item.attachments[0]['photo']['photo_604']
            })
        }
    })
    return result
}

function appendHeader(parent, date) {
    let header = document.createElement('p')
    header.setAttribute('class', 'header')
    header.appendChild(document.createTextNode(`Обновление от ${formatDate(date)}`))
    parent.appendChild(header)
}

function appendBody(parent, news) {
    let mainText = news.text.replace(/#Обновление|#обновление|:/g, '').trim()
    let image = document.createElement('img')
    image.setAttribute('src', news.image)
    parent.appendChild(image)
    let bodyContent = document.createElement('p')
    bodyContent.setAttribute('class', 'main-text')
    bodyContent.appendChild(document.createTextNode(mainText))
    let body = document.createElement('div')
    body.appendChild(bodyContent)    
    if (news.link.length > 0) {
        let link = document.createElement('a')
        link.innerHTML = 'Читать'
        link.setAttribute('class', 'article-btn')
        link.setAttribute('href', news.link)
        link.setAttribute('target', '_blank')
        body.appendChild(link)
    }
    body.setAttribute('class', 'news-body')
    parent.appendChild(body)    
}

function appendStatistic(parent, news) {
    let statisticBlock = document.createElement('div')    
    statisticBlock.setAttribute('class', 'news-stat')

    let views = document.createElement('i')
    views.appendChild(document.createTextNode(`${news.views}`))
    views.setAttribute('class', 'views fas fa-eye')    
    statisticBlock.appendChild(views)

    let likes = document.createElement('i')
    likes.appendChild(document.createTextNode(`${news.likes}`))
    likes.setAttribute('class', 'likes far fa-heart')
    statisticBlock.appendChild(likes)

    parent.appendChild(statisticBlock)
}

function formatDate(seconds) {
    var formattedDate = new Date(1970, 0, 1)
    formattedDate.setSeconds(seconds)
    return formattedDate.toLocaleDateString()
}