const path = require('path')
const webpack = require('webpack')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const CleanLessPlugin = require("less-plugin-clean-css")

const extractLess = new ExtractTextPlugin({
    filename: 'css/[name].css'
})

let NODE_ENV;
if (process.env.NODE_ENV) {
    NODE_ENV = process.env.NODE_ENV.replace(/^\s+|\s+$/g, "")
}

const config = {
    context: __dirname,
    entry: {
        site: ['./styles/site.less'],
        landing: ['./scripts/landing.js', './styles/landing.less'],
        auth: ['./scripts/auth/login.js'],
        donate: './scripts/donate.js',
        news: './scripts/news.js',
        cabinet: ['./cabinet/app/app.js']
    },
    output: {
        path: path.resolve(__dirname, 'build'),
        publicPath: 'build/',
        filename: 'js/[name].js',
        library: '[name]',
    },
    devServer: {
        inline: true,
        hot: true,
        contentBase: './'
    },
    module: {
        rules: [{
            test: /\.js$/,
            exclude: [/node_modules/],
            loader: 'babel-loader',
            query: {
                presets: ['react', 'es2015']
            }
        }, {
            test: /\.less$/,
            exclude: [/node_modules/],
            use: ['css-hot-loader'].concat(extractLess.extract({
                fallback: 'style-loader',
                use: [{
                    loader: 'css-loader'
                }, 
                {
                    loader: 'less-loader',
                    options: {
                        plugins: [
                            new CleanLessPlugin({ advanced: true })
                        ]
                    }
                }]
            }))
        }]
    },
    watch: NODE_ENV == 'dev',
    devtool: NODE_ENV == 'dev' && 'eval',
    plugins: [
        new webpack.EnvironmentPlugin(['NODE_ENV']),
        new webpack.optimize.UglifyJsPlugin({
            compress: { warnings: false }
        }),
        new webpack.ProvidePlugin({
            axios: 'axios'
        }),
        extractLess
    ]
}

module.exports = config