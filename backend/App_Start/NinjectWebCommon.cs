﻿using System;
using System.Web;
using backend;
using backend.Managers;
using backend.Managers.Interfaces;
using backend.Providers;
using backend.Providers.Interfaces;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Ninject;
using Ninject.Syntax;
using Ninject.Web.Common;
using Ninject.Web.Common.WebHost;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethod(typeof(NinjectWebCommon), "Stop")]

namespace backend {
    public static class NinjectWebCommon {
        private static readonly Bootstrapper _bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            _bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop() {
            _bootstrapper.ShutDown();
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        private static IKernel CreateKernel() {
            var kernel = new StandardKernel();
            kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
            kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

            RegisterServices(kernel);
            return kernel;
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        private static void RegisterServices(IBindingRoot kernel) {
            kernel.Bind<IPaymentsProvider>().To<PaymentsProvider>();
            kernel.Bind<IAccountsProvider>().To<AccountsProvider>();
            kernel.Bind<IPlayersInfoProvider>().To<PlayersInfoProvider>();

            kernel.Bind<IPaymentsManager>().To<PaymentsManager>();
            kernel.Bind<IAccountManager>().To<AccountManager>();
            kernel.Bind<IShopManager>().To<ShopManager>();
        }

        /// <summary>
        /// Возвращает реализацию
        /// </summary>
        public static T Get<T>() where T : class {
            return _bootstrapper?.Kernel?.Get<T>();
        }
    }
}