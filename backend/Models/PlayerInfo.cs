﻿using System;
using gta_mp_data.Entity;

namespace backend.Models {
    /// <summary>
    /// Модель данных игрока
    /// </summary>
    public class PlayerInfo {
        /// <summary>
        /// Уникальный идентификатор
        /// </summary>
        public long AccountId { get; set; }

        /// <summary>
        /// Игровой ник
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Цвет ника
        /// </summary>
        public string NameColor { get; set; }

        /// <summary>
        /// Почта
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Баланс аккаунта
        /// </summary>
        public int Balance { get; set; }

        /// <summary>
        /// Деньги в игре
        /// </summary>
        public int Money { get; set; }

        /// <summary>
        /// Опыт
        /// </summary>
        public int Experience { get; set; }

        /// <summary>
        /// Уровень в игре
        /// </summary>
        public int Level { get; set; }

        /// <summary>
        /// Номер телефона
        /// </summary>
        public int Phone { get; set; }

        /// <summary>
        /// Информация о клане
        /// </summary>
        public PlayerClanInfo ClanInfo { get; set; }

        /// <summary>
        /// Время окончания премиума
        /// </summary>
        public DateTime PremiumEnd { get; set; }

        /// <summary>
        /// Последний вход
        /// </summary>
        public DateTime LastLogin { get; set; }

        /// <summary>
        /// Всего часов проведено в игре
        /// </summary>
        public int TotalInGame { get; set; }
    }
}