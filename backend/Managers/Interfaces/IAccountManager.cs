﻿using backend.Models;

namespace backend.Managers.Interfaces {
    public interface IAccountManager {
        /// <summary>
        /// Возвращает суммарную информацию об аккаунте
        /// </summary>
        PlayerInfo GetInfo(string email);

        /// <summary>
        /// Обновить баланс аккаунта
        /// </summary>
        bool UpdateBalance(string email, int balance);

        /// <summary>
        /// Проверяет наличие аккаунта
        /// </summary>
        bool IsAccountExist(string email);

        /// <summary>
        /// Создает новый аккаунт
        /// </summary>
        OperationResult Register(string email, string password, string referal);
    }
}