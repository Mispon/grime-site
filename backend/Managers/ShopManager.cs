﻿using System;
using backend.Managers.Interfaces;
using backend.Models;
using backend.Providers.Interfaces;

namespace backend.Managers {
    /// <summary>
    /// Логика обработки покупок
    /// </summary>
    public class ShopManager : IShopManager {
        private readonly IAccountsProvider _accountsProvider;
        private readonly IPlayersInfoProvider _playersInfoProvider;

        public ShopManager(IAccountsProvider accountsProvider, IPlayersInfoProvider playersInfoProvider) {
            _accountsProvider = accountsProvider;
            _playersInfoProvider = playersInfoProvider;
        }

        /// <summary>
        /// Покупка премиума
        /// </summary>
        public OperationResult BuyPremium(long accountId, int cost, int days) {
            var account = _accountsProvider.Get(accountId);
            if (account.Balance < cost) {
                return new OperationResult {Success = false, Message = "Недостаточно денег для покупки"};
            }
            account.Balance -= cost;
            account.PremiumEnd = DateTime.Now.AddDays(days);
            var saveResult = _accountsProvider.Set(account);
            return saveResult
                ? new OperationResult {Success = true, Message = $"Премиум-аккаунт активирован до {account.PremiumEnd:g}"}
                : GetFailedResult();
        }

        /// <summary>
        /// Смена имени персонажа
        /// </summary>
        public OperationResult ChangeName(long accountId, int cost, string newName) {
            if (!EnoughBalance(accountId, cost, out var checkResult)) {
                return checkResult;
            }
            if (!_playersInfoProvider.NameAvailable(newName)) {
                return new OperationResult {Success = false, Message = "Выбранное имя уже занято"};
            }
            var playerInfo = _playersInfoProvider.Get(accountId);
            playerInfo.Name = newName;
            var result = _playersInfoProvider.Set(playerInfo);
            result &= _accountsProvider.UpdateBalance(accountId, -cost);
            return result
                ? new OperationResult {Success = true, Message = "Имя персонажа успешно изменено"}
                : GetFailedResult();
        }

        /// <summary>
        /// Смена имени персонажа
        /// </summary>
        public OperationResult ChangeNameColor(long accountId, int cost, string color) {
            if (!EnoughBalance(accountId, cost, out var checkResult)) {
                return checkResult;
            }
            var playerInfo = _playersInfoProvider.GetAdditional(accountId);
            playerInfo.TagColor = color;
            var result = _playersInfoProvider.SetAdditional(playerInfo);
            result &= _accountsProvider.UpdateBalance(accountId, -cost);
            return result
                ? new OperationResult { Success = true, Message = "Цвет ника успешно изменен" }
                : GetFailedResult();
        }

        /// <summary>
        /// Смена имени персонажа
        /// </summary>
        public OperationResult ChangePhoneNumber(long accountId, int cost, int phoneNumber) {
            if (!EnoughBalance(accountId, cost, out var checkResult)) {
                return checkResult;
            }
            if (!_playersInfoProvider.NumberAvailable(phoneNumber)) {
                return new OperationResult {Success = false, Message = "Выбранный номер уже занят"};
            }
            var playerInfo = _playersInfoProvider.GetAdditional(accountId);
            playerInfo.PhoneNumber = phoneNumber;
            var result = _playersInfoProvider.SetAdditional(playerInfo);
            result &= _accountsProvider.UpdateBalance(accountId, -cost);
            return result
                ? new OperationResult {Success = true, Message = "Номер телефона успешно изменен"}
                : GetFailedResult();
        }

        /// <summary>
        /// Сброс внешности
        /// </summary>
        public OperationResult ResetSkin(long accountId, int cost) {
            if (!EnoughBalance(accountId, cost, out var checkResult)) {
                return checkResult;
            }
            var result = _playersInfoProvider.ResetName(accountId);
            result &= _accountsProvider.UpdateBalance(accountId, -cost);
            return result
                ? new OperationResult {Success = true, Message = "Настройки внешности успешно сброшены"}
                : GetFailedResult();
        }

        /// <summary>
        /// Добавляет временные скины в инвентарь
        /// </summary>
        public OperationResult AddTemporarySkin(long accountId, int cost, int count) {
            if (!EnoughBalance(accountId, cost, out var checkResult)) {
                return checkResult;
            }
            var result = _playersInfoProvider.AddTempSkins(accountId, count);
            result &= _accountsProvider.UpdateBalance(accountId, -cost);
            return result
                ? new OperationResult { Success = true, Message = "Временные скины успешно добавлены в инвернарь" }
                : GetFailedResult();
        }

        /// <summary>
        /// Проверка баланса аккаунта
        /// </summary>
        private bool EnoughBalance(long accountId, int cost, out OperationResult result) {
            if (!_accountsProvider.EnoughBalance(accountId, cost)) {
                result = new OperationResult {Success = false, Message = "Недостаточно денег для покупки"};
                return false;
            }
            result = null;
            return true;
        }

        /// <summary>
        /// Возвращает ответ в случае системной ошибки
        /// </summary>
        private static OperationResult GetFailedResult() {
            return new OperationResult {Success = false, Message = "Не удалось обновить данные. Обратитесь к администрации."};
        }
    }
}