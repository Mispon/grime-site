﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using backend.Providers.Interfaces;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;

namespace backend {
    public class ApplicationOAuthProvider : OAuthAuthorizationServerProvider {
        private readonly IAccountsProvider _accountsProvider;
        private readonly string _publicClientId;

        public ApplicationOAuthProvider(string publicClientId, IAccountsProvider accountsProvider) {
            if (string.IsNullOrEmpty(publicClientId)) {
                throw new ArgumentNullException(nameof(publicClientId));
            }
            _publicClientId = publicClientId;
            _accountsProvider = accountsProvider;
        }

        /// <summary>
        /// Метод генерации токена
        /// </summary>
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context) {
            var account = await _accountsProvider.GetAccountAsync(context.UserName, context.Password);
            if (account == null) return;
            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            identity.AddClaim(new Claim(ClaimTypes.Name, account.Email));
            var properties = new AuthenticationProperties(new Dictionary<string, string> {["email"] = account.Email});
            var ticket = new AuthenticationTicket(identity, properties);
            context.Validated(ticket);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context) {
            foreach (var property in context.Properties.Dictionary) context.AdditionalResponseParameters.Add(property.Key, property.Value);
            return Task.FromResult<object>(null);
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context) {
            if (context.ClientId == null) context.Validated();
            return Task.FromResult<object>(null);
        }

        public override Task ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context) {
            if (context.ClientId == _publicClientId) {
                var expectedRootUri = new Uri(context.Request.Uri, "/");
                if (expectedRootUri.AbsoluteUri == context.RedirectUri) context.Validated();
            }
            return Task.FromResult<object>(null);
        }
    }
}