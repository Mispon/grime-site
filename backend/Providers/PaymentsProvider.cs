﻿using System.Linq;
using backend.Entities;
using backend.Providers.Interfaces;
using LinqToDB;
// ReSharper disable AccessToDisposedClosure

namespace backend.Providers {
    /// <summary>
    /// Провайдер к данным платежей
    /// </summary>
    public class PaymentsProvider : BaseProvider, IPaymentsProvider {
        /// <summary>
        /// Проверяет наличие платежа
        /// </summary>
        public bool IsExist(string initId) {
            using (var db = new Database()) {
                return db.Payments.Any(e => e.InitId == initId);
            }
        }

        /// <summary>
        /// Добавляет запись о новом платеже
        /// </summary>
        public bool Add(Payment payment) {
            using (var db = new Database()) {
                return SafetyExecute(() => db.Insert(payment));
            }
        }

        /// <summary>
        /// Возвращает идентификатор последнего платежа
        /// </summary>
        public int GetLastPaymentId() {
            using (var db = new Database()) {
                return db.Payments.OrderByDescending(e => e.Id).FirstOrDefault()?.Id ?? 0;
            }
        }
    }
}