﻿using gta_mp_data.Entity;

namespace backend.Providers.Interfaces {
    public interface IPlayersInfoProvider {
        /// <summary>
        /// Возвращает основные данные игрока
        /// </summary>
        PlayerInfo Get(long accountId);

        /// <summary>
        /// Записывает данные игрока
        /// </summary>
        bool Set(PlayerInfo playerInfo);

        /// <summary>
        /// Возвращает дополнительные данные игрока
        /// </summary>
        PlayerAdditionalInfo GetAdditional(long accountId);

        /// <summary>
        /// Записывает дополнительные данные игрока
        /// </summary>
        bool SetAdditional(PlayerAdditionalInfo playerAdditionalInfo);

        /// <summary>
        /// Сбрасывает имя персонажа
        /// </summary>
        bool ResetName(long accountId);

        /// <summary>
        /// Проверяет, что имя доступно
        /// </summary>
        bool NameAvailable(string name);

        /// <summary>
        /// Проверяет, что номер доступен
        /// </summary>
        bool NumberAvailable(int number);

        /// <summary>
        /// Добавляет временный скин в инвентарь
        /// </summary>
        bool AddTempSkins(long accountId, int count);
    }
}