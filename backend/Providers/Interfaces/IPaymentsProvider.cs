﻿using backend.Entities;

namespace backend.Providers.Interfaces {
    public interface IPaymentsProvider {
        /// <summary>
        /// Проверяет наличие платежа
        /// </summary>
        bool IsExist(string initId);

        /// <summary>
        /// Добавляет запись о новом платеже
        /// </summary>
        bool Add(Payment payment);

        /// <summary>
        /// Возвращает идентификатор последнего платежа
        /// </summary>
        int GetLastPaymentId();
    }
}