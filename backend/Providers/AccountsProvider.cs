﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using backend.Providers.Interfaces;
using gta_mp_data.Entity;
using gta_mp_data.Enums;
using LinqToDB;
using PlayerInfo = backend.Models.PlayerInfo;

// ReSharper disable AccessToDisposedClosure

namespace backend.Providers {
    /// <summary>
    /// Провайдер к данным аккаунтов
    /// </summary>
    public class AccountsProvider : BaseProvider, IAccountsProvider {
        /// <summary>
        /// Получить аккаунт по почте и паролю
        /// </summary>
        public Task<Account> GetAccountAsync(string email, string password) {
            return Task.Run(() => {
                using (var db = new Database()) {
                    return db.Accounts.FirstOrDefault(e => e.Email == email && e.Password == password);
                }
            });
        }

        /// <summary>
        /// Получить данные аккаунта
        /// </summary>
        public Account Get(long accountId) {
            using (var db = new Database()) {
                return db.Accounts.FirstOrDefault(e => e.Id == accountId);
            }
        }

        /// <summary>
        /// Записать данные аккаунта
        /// </summary>
        public bool Set(Account account) {
            using (var db = new Database()) {
                return SafetyExecute(() => db.Update(account));
            }
        }

        /// <summary>
        /// Возвращает суммарную информацию об аккаунте
        /// </summary>
        public PlayerInfo GetInfo(string email) {
            using (var db = new Database()) {
                var account = db.Accounts.First(e => e.Email == email);
                var playerInfo = db.PlayerInfos.First(e => e.AccountId == account.Id);
                var additionalInfo = db.PlayerAdditionalInfos.First(e => e.AccountId == account.Id);
                var clanInfo = db.ClanInfos.FirstOrDefault(e => e.AccountId == account.Id);
                var money = db.Inventory.First(e => e.OwnerId == account.Id && e.Type == InventoryType.Money);
                return new PlayerInfo {
                    AccountId = account.Id,
                    Name = playerInfo.Name,
                    NameColor = additionalInfo.TagColor,
                    Email = account.Email,
                    Balance = account.Balance,
                    Money = money.Count + money.CountInHouse,
                    Experience =  playerInfo.Experience,
                    Level = playerInfo.Level,
                    Phone = additionalInfo.PhoneNumber,
                    ClanInfo = clanInfo,
                    PremiumEnd = account.PremiumEnd,
                    LastLogin = account.LastLogin,
                    TotalInGame = (int) new TimeSpan(account.TotalTime).TotalHours
                };
            }
        }

        /// <summary>
        /// Проверяет существование аккаунта
        /// </summary>
        public bool IsAccountExist(string email) {
            using (var db = new Database()) {
                return db.Accounts.Any(e => e.Email == email);
            }
        }

        /// <summary>
        /// Обновить баланс аккаунта по идентификатору
        /// </summary>
        public bool UpdateBalance(long accountId, int balance) {
            return UpdateBalance(e => e.Id == accountId, balance);
        }

        /// <summary>
        /// Обновить баланс аккаунта по почте
        /// </summary>
        public bool UpdateBalance(string email, int balance) {
            return UpdateBalance(e => e.Email == email, balance);
        }

        /// <summary>
        /// Обновить баланс аккаунта
        /// </summary>
        private static bool UpdateBalance(Expression<Func<Account, bool>> predicate, int balance) {
            using (var db = new Database()) {
                return SafetyExecute(
                    () => db.Accounts
                        .Where(predicate)
                        .Set(e => e.Balance, e => e.Balance + balance)
                        .Update()
                );
            }
        }

        /// <summary>
        /// Проверяет, недостаточно ли денег на балансе
        /// </summary>
        public bool EnoughBalance(long accountId, int cost) {
            using (var db = new Database()) {
                return db.Accounts.First(e => e.Id == accountId).Balance >= cost;
            }
        }

        /// <summary>
        /// Добавляет новый аккаунт
        /// </summary>
        public bool AddAccount(string email, string password, string friendReferal) {
            return SafetyExecute(
                () => {
                    using (var db = new Database()) {
                        var account = new Account {Email = email, Password = password, LastLogin = DateTime.MinValue, FriendReferal = friendReferal};
                        account.Id = db.InsertWithInt64Identity(account);
                        var playerInfo = new gta_mp_data.Entity.PlayerInfo {AccountId = account.Id, Health = 100, Satiety = 100};
                        var additionalInfo = new PlayerAdditionalInfo {AccountId = account.Id};
                        var driverInfo = new DriverInfo {AccountId = account.Id};
                        var wantedInfo = new Wanted {AccountId = account.Id};
                        var settings = new Settings {AccountId = account.Id};
                        var inventory = new InventoryItem {OwnerId = account.Id, Name = "Деньги", Type = InventoryType.Money, Count = 100, CountInHouse = 0};
                        db.Insert(playerInfo);
                        db.Insert(additionalInfo);
                        db.Insert(driverInfo);
                        db.Insert(wantedInfo);
                        db.Insert(settings);
                        db.Insert(inventory);
                    }
                }
            );
        }
    }
}