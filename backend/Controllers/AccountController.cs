﻿using System.Web.Http;
using backend.Managers.Interfaces;

namespace backend.Controllers {
    /// <summary>
    /// Обработчик запросов к аккаунту
    /// </summary>
    public class AccountController : ApiController {
        private readonly IAccountManager _accountManager;
        private readonly IPaymentsManager _paymentsManager;

        public AccountController(IAccountManager accountManager, IPaymentsManager paymentsManager) {
            _accountManager = accountManager;
            _paymentsManager = paymentsManager;
        }

        /// <summary>
        /// Регистрация нового аккаунта
        /// </summary>
        [HttpGet]
        [Route("api/account/getInfo")]
        public IHttpActionResult GetInfo(string email) {
            var info = _accountManager.GetInfo(email);
            return Json(info);
        }

        /// <summary>
        /// Регистрация нового аккаунта
        /// </summary>
        [HttpPost]
        [Route("api/account/register")]
        public IHttpActionResult Register(string email, string password, string referal) {
            var result = _accountManager.Register(email, password, referal);
            return Json(result);
        }

        /// <summary>
        /// Смена пароля
        /// </summary>
        [HttpPost]
        [Route("api/account/changePassword")]
        public IHttpActionResult ChangePassword(string email, string newPassword) {
            // todo: хранить в базе "почта-код-срок истечения"
            return Json(false);
        }

        /// <summary>
        /// Проверяет наличие аккаунта по почте
        /// </summary>
        [HttpGet]
        [Route("api/account/checkAccount")]
        public IHttpActionResult CheckAccount(string email) {
            var result = _accountManager.IsAccountExist(email);
            var orderId = -1;
            if (result) {
                orderId = _paymentsManager.GetLastPaymentId();
            }
            return Json(new {success = result, orderId = ++orderId});
        }
    }
}